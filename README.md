## geomancy-layout

Layout primitives using [geomancy] vectors.

* `Geomancy.Layout` - box cutting and stacking.
* `Geomancy.Layout.Box` - size/position and top/right/bottom/left boxes and manipulation.
* `Geomancy.Layout.Alignment` - distribute size leftovers proportionally.

[geomancy]: https://hackage.haskell.org/package/geomancy
