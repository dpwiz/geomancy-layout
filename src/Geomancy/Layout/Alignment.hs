module Geomancy.Layout.Alignment where

import Geomancy

-- | @left/center/right@ & @top/middle/bottom@
type Alignment = Vec2

leftTop :: Alignment
leftTop = vec2 Begin Begin

leftMiddle :: Alignment
leftMiddle = vec2 Begin Middle

leftBottom :: Alignment
leftBottom = vec2 Begin End

centerTop :: Alignment
centerTop = vec2 Middle Begin

center :: Alignment
center = vec2 Middle Middle

centerBottom :: Alignment
centerBottom = vec2 Middle End

rightTop :: Alignment
rightTop = vec2 End Begin

rightMiddle :: Alignment
rightMiddle = vec2 End Middle

rightBottom :: Alignment
rightBottom = vec2 End End

type Origin = Float

pattern Begin :: Origin
pattern Begin = 0.0

pattern Middle :: Origin
pattern Middle = 0.5

pattern End :: Origin
pattern End = 1.0

{- | Distribute size difference according to origin.

@
(before, after) = placeSize1d _origin size target
before + size + after === target

placeBegin = placeSize1d 0
(0.0, 1.0) = placeBegin 1.0 2.0

placeMiddle = placeSize1d 0.5
(1.0, 1.0) = placeMiddle 1.0 3.0

placeEnd = placeSize1d 1.0
(1.0, 0.0) = placeEnd 1.0 2.0
@
-}
{-# INLINE placeSize1d #-}
placeSize1d :: Origin -> Float -> Float -> (Float, Float)
placeSize1d origin size target = (before, after)
  where
    leftovers = target - size
    before = leftovers * origin
    after = leftovers - before
